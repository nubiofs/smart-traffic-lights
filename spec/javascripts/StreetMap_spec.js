//= require StreetMap
//= require StreetSection

describe("StreetMap object", function() {
    
    var zoom = 15;
    var element  = document.createElement('div');
    var path = [{lat:-10, lng: -10}, {lat:-10, lng: -11}];
    var map = new StreetMap(path, element, zoom);


    it("should draw the correct number of traffic lights", function(){
        var expectedLength = 41;
        expect(map.drawTrafficLights()).toBeTruthy;
    });
    
    it("should be able to draw the street sections", function() {
        var expectedLength = 52;
        expect(map.drawStreetSections()).toBeTruthy();
    });

    
    it("should change the zoom value", function(){
        map.zoomChanged()();
        
        for (var i in map.trafficLights) {
            expect(map.trafficLights[i].light.icon).not.toBe(null);
        }
    });
    
    it("should close all the infoWindows from street sections", function(){
        map.closeInfoWindows();
        for (var i in map.streetSections) {
            expect(map.streetSections[i].infoWindow.map).toBe(null);
    
        }
    });


});
