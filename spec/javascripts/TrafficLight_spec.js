//= require TrafficLight
//= require StreetMap

describe("TrafficLight object", function() {

	var mapElement  = document.createElement('div');
    var mapLocation = {lat: -23.558147, lng: -46.660198};
    var mapZoom = 15;
    var color = "green";
    var map = new StreetMap(mapLocation, mapElement, mapZoom);
    var light = new TrafficLight(mapLocation, mapZoom, map, color);

    it("should belong to a map", function() {
    	expect(light.map).toBe(map);
    });

    it("should go initially with green color", function() {
    	expect(light.color).toEqual("green");
    });

    it("may change its color when necessary", function() {
    	var color = "red";
    	light.changeColor(color);
    	expect(light.color).toEqual(color);
    });

    it("should become invisible if a user said so", function() {
    	light.setVisibility(false);
    	expect(light.light.visible).toBeFalsy();
    });

    it("should become visible again if a user change her mind", function() {
    	light.setVisibility(true);
    	expect(light.light.visible).toBeTruthy();
    });

    it("should change its appearance when zoom is small", function() {
    	var result = light.chooseIcon(15, "yellow");
    	expect(result).toEqual("assets/semaphore_yellow_small.png");
    });

    it("should change its appearance when zoom is medium", function() {
    	var result = light.chooseIcon(18, "red");
    	expect(result).toEqual("assets/semaphore_red_medium.png");
    });

    it("should change its appearance when zoom is large", function() {
    	var result = light.chooseIcon(20, "green");
    	expect(result).toEqual("assets/semaphore_green_big.png");
    });
});