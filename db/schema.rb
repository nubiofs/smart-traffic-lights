# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160603040442) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actuator_sensors", force: :cascade do |t|
    t.integer  "actuator_id"
    t.integer  "sensor_id"
    t.boolean  "direct_influence"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "actuators", force: :cascade do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.string   "resource"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sensors", force: :cascade do |t|
    t.integer  "street_section_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "resource"
    t.float    "speed"
    t.float    "density"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "street_sections", force: :cascade do |t|
    t.float    "latitude_start"
    t.float    "latitude_end"
    t.float    "longitude_start"
    t.float    "longitude_end"
    t.string   "cam_resource"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "traffic_lights", force: :cascade do |t|
    t.integer  "actuator_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "street"
    t.string   "way"
    t.integer  "img_rotation"
    t.boolean  "init_state"
    t.string   "init_img"
    t.integer  "sensor_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

end
