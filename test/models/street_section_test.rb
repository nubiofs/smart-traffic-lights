require 'test_helper'

class StreetSectionTest < ActiveSupport::TestCase

  test "street sections must be on different locations" do
  	init_lat1 = StreetSection.find(1).latitude_start
  	init_lat2 = StreetSection.find(2).latitude_start
  	end_lat1  = StreetSection.find(1).latitude_end
  	end_lat2  = StreetSection.find(2).latitude_end 
    assert_not_equal init_lat1, init_lat2
    assert_not_equal end_lat1, end_lat2
  end

  test "latitude_start is a float" do
    assert_kind_of 1.0.class, StreetSection.find(1).latitude_start
  end

  test "longitude_start is a float" do
    assert_kind_of 1.0.class, StreetSection.find(1).longitude_start
  end

  test "latitude_end is a float" do
    assert_kind_of 1.0.class, StreetSection.find(1).latitude_end
  end

  test "longitude_end is a float" do
    assert_kind_of 1.0.class, StreetSection.find(1).longitude_end
  end

  test "cam resource is a string" do
  	assert_kind_of "address".class, StreetSection.find(1).cam_resource
  end
end
