/*=======================================
 *
 * TrafficLight class
 *
 *---------------------------------------
 * This object represents a traffic light
 * icon that is drawn on the map. Its main
 * purpose is store state information
 * (color, position, visibility) so this
 * can be used to portray it graphically
 * on page.
 ========================================*/

var TrafficLight = function(location, zoom, map, color, street, way, id) {
    this.map = map;
    this.id = id;
    this.color = color;
    this.street = street;
    this.way = way;
    this.zoom = zoom;
    
    this.light = new google.maps.Marker({
        position: location,
        map: this.map.map,
        icon: this.chooseIcon(zoom, color)
    });

    this.infoWindow = new google.maps.InfoWindow();
    this.light.addListener('click', this.setInfoWindow());
}

TrafficLight.prototype = {
    
    constructor: TrafficLight,
    
    changeColor: function(color) {
        var tl = this;
        tl.color = color;
        $.get("traffic_light/edit/"+tl.id).success(function(lights){
            var icon = tl.chooseIcon(tl.zoom, color);
            tl.light.setIcon(icon);
        });

    },

    setVisibility: function(boolean) {
        this.light.setOptions({visible: boolean});
    },
    
    chooseIcon: function(zoom, color) {
        this.zoom = zoom;
        return "assets/semaphore_" + color + (zoom <= 17 ? "_small.png" :
                                             (zoom <= 19 ? "_medium.png" :
                                                           "_big.png"));
    },
    
    setInfoWindow: function() {
        var light = this;
        return function(e) {
            var contentString = '<div class="InfoWindowText">' +
                                  '<p> Paulista Avenue with ' + light.street +' Street (way to ' + light.way + ')</p>' +
                                  '<p> Traffic Light is ' + light.color + '</p>' +
                                '</div>';
            light.map.closeInfoWindows();
            light.infoWindow.setOptions({
                content: contentString,
                position: e.latLng
            });
            light.infoWindow.open(light.map.map);
        }
    },

    closeInfoWindow: function() {
        this.infoWindow.close();
    }
}